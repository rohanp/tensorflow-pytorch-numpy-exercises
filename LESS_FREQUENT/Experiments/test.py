from ezpznet.sketchgan import SketchGAN, load_image

image_path = "./image.jpg"

image = load_image(image_path)
net = SketchGAN()
pred = net.predict(image)

plt.imshow(pred.squeeze(), cmap="gray")
