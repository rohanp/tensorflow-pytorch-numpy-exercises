import React, { Fragment, Suspense, lazy } from "react"
import { MuiThemeProvider, CssBaseline } from "@material-ui/core"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import theme from "./theme"
import GlobalStyles from "./GlobalStyles"
import Pace from "./shared/components/Pace"

const LoggedOutComponent = lazy(() => import("./components/Main"))

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <MuiThemeProvider theme={theme}>
          <CssBaseline />
          <GlobalStyles />
          <Pace color={theme.palette.primary.light} />
          <Suspense fallback={<Fragment />}>
            <Switch>
              <Route path="/" component={LoggedOutComponent}></Route>
            </Switch>
          </Suspense>
        </MuiThemeProvider>
      </Switch>
    </BrowserRouter>
  )
}

export default App
