1. Convert NB to MD
2. Paste MD to Online HTML Converter
3. Paste block by block to my Blog
4. For Code Highlight use `import SyntaxHighlighter from "react-syntax-highlighter"`

## MAIN PROBLEM WITH THIS WORKFLOW

1. I have to paste the long html only block by block as pasting the whole HTML will definitely NOT work.

2. Need to close each <img> tag, as React / JSX will not accept non-closing <img> tag, But most online md-to-html converter will not put the closing tag to the <img>

---

```js
<YouTube videoId="QI0qjDfMtAw" opts={opts} onReady={this._onReady} />
```

## Example

```js

import React, { Component, Fragment } from "react"
import { Typography } from "@material-ui/core"
import YouTube from "react-youtube"
import SyntaxHighlighter from "react-syntax-highlighter"
import { dark } from "react-syntax-highlighter/dist/esm/styles/hljs"
import Markdown from "react-markdown"

class MSMalwareDetection extends Component {
  render() {
    const opts = {
      height: "440",
      width: "810",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 0, // If 1 it will start playing automatically
      },
    }

    const codeString = "(num) => num + 1"

    return (
      <Fragment>
        <Typography paragraph>
          <div>
            <h2>
              Video in my Channel explaining the entire work I did on Microsoft
              Malware Detection Kaggle Challenge — BIG-2015
            </h2>
            <YouTube
              videoId="QI0qjDfMtAw"
              opts={opts}
              onReady={this._onReady}
            />

            <p>
              <a href="https://github.com/rohan-paul/MachineLearning-DeepLearning-Code-for-my-YouTube-Channel/blob/master/Kaggle_Competition/Microsoft_Malware_Classification_BIG_2015/Microsoft_Malware_BIG_2015_Kaggle.ipynb">
                <strong>Link to Github with full code</strong>
              </a>
            </p>

            <SyntaxHighlighter language="javascript" style={dark}>
              {`
              # asmoutputfile.csv(output genarated from the above two cells)
              # will contain all the extracted features from .asm files
              # we will use this file directly dfasm=pd.read_csv("asmoutputfile.csv")

              Y.columns = ['ID', 'Class'] result_asm = pd.merge(dfasm,

              Y,on='ID', how='left') result_asm.head()
              `}
            </SyntaxHighlighter>

            <p>
              One of the largest public available data sets with malware can be
              found in the &nbsp;
              <a href="https://www.kaggle.com/c/malware-classification/overview">
                <strong>Microsoft Malware Classification Challenge</strong>
              </a>
              . It consists of over 400 GB of data, with both binary and
              disassembled code from the use of the IDA disassembler and
              debugger.3 The binary malware has been stripped of the PE-header
              to be made non-executable for security reasons. This does limit
              the value of the data set, but they have prioritized the potential
              security implications with hundreds of gigabytes of executable.
              malware available to anyone. The MMCC data set consists of around
              200 GB of training data, where the classification is known, and
              another 200 GB of test data to be classified as a part of the
              competition evaluation.
            </p>
            </div>
        </Typography>
      </Fragment>

```
