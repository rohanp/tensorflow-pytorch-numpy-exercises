## Workflow - NB => MD => Render this MD itself

### MAIN PROBLEM IN THIS APPROACH

Code highlighting is broken.

Implemented this - https://stackoverflow.com/a/68179028/1902852

But its Broken

---

The Code highlight is slightly more difficult

1. Convert my full Jupyter NB to MD with

```
jupyter nbconvert --to markdown mynotebook.ipynb
```

2. Replace all "`python" and "`py" with "<pre>"

3. Replace all corresponding "```" with "</pre>"

4. Now in my Blog Component - I will have below

```js
import React, { Component, Fragment } from "react"
import { Typography } from "@material-ui/core"
import YouTube from "react-youtube"
import SyntaxHighlighter from "react-syntax-highlighter"
import { dark } from "react-syntax-highlighter/dist/esm/styles/hljs"
import ReactMarkdown from "react-markdown"
import Markdown from "markdown-to-jsx"

class MSMalwareDetection extends Component {
  render() {
    const opts = {
      height: "440",
      width: "810",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 0, // If 1 it will start playing automatically
      },
    }

    const codeString = "(num) => num + 1"
    return (
      <Fragment>
        <YouTube videoId="QI0qjDfMtAw" opts={opts} onReady={this._onReady} />
        <Markdown>{`
## First the Fundamental of an Image Structure in Computer


![Imgur](https://imgur.com/Ia6zn53.png)

<pre>
from PIL import Image, ImageDraw
import numpy as np
from numpy import asarray

image = Image.open('image.jpg')

# Show image
# image.show()
display(image)

</pre>

![png](GrayScale_Brightness_files/GrayScale_Brightness_2_0.png)

### Check any pixel value in the image


`}</Markdown>
      </Fragment>
    )
  }
}

export default MSMalwareDetection
```

5. Finally check the rendered blog overall. Some comments inside code starting with "#" may have been rendered like <h1> text. Just select those comments and press tab once to move them 1 tab to the right - and they should be corrected.
