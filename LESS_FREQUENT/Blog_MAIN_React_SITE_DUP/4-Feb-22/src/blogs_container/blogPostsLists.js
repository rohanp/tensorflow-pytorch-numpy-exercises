import GWDetectionPage from "./each_blog_posts/Gravitational_Wave_Detection_Kaggle_Challenge_Part_1/gravitationalWavesDetectionKaggle"
import MSMalwareDetection from "./each_blog_posts/MSMalware_Sept_24_2021/microsoftMalwareDetection"
import GrayScaleBrightness from "./each_blog_posts/grayscale_brightness_nov_2_2021/GrayScaleBrightness"
import GravitationalWaves2 from "./each_blog_posts/Gravitational_Wave_Detection_Kaggle_Challenge_Part_2/GravitationalWaves2"
import SantanderValuePredictionKaggle from "./each_blog_posts/Santander-Kg-Value-Prediction-Challenge/SantanderValuePredictionKaggle"
import TensorflowMixedPrecisionTrainingCIFAR10 from "./each_blog_posts/Tensorflow_Mixed_Precision_Training_CIFAR10_dataset_Implementation/TensorflowMixedPrecisionTrainingCIFAR10"
import TFCustomLayersFundamentalBuildingBlocksForTrainingModel from "./each_blog_posts/TF-Custom-Layers-Building-Blocks-For-Training-17-Nov-2021/TFCustomLayersFundamentalBuildingBlocksForTrainingModel"
import RandomSearchCVFromScratch from "./each_blog_posts/RandomSearchCV_From_Scratch/RandomSearchCV_From_Scratch"
import GridSearchCVFromScratch from "./each_blog_posts/GridSearchCV_From_Scratch/GridSearchCVFromScratch"
import SGDFromScratch from "./each_blog_posts/SGD_Classifier_From_Scratch_without_scikit-learn/SGDFromScratch"
import ProbabilityProblemSolution1 from "./each_blog_posts/Probability-Problem-Solution-series_1/ProbabilityProblemSolutionSeries1"
import CategoricalVariablesHandling from "./each_blog_posts/Categorical_Variables_Handling/CategoricalVariablesHandling"
import BtcEdaMovingAverage from "./each_blog_posts/BTC_EDA-Moving-Average/BtcEdaMovingAverage"
import BtcLSTM from "./each_blog_posts/Btc_Lstm/BtcLstm"
import MeanMedianMode from "./each_blog_posts/MeanMedianMode/MeanMedianMode"
import YfinanceTeslaStockPredictionSarimax from "./each_blog_posts/yfinanceTeslaStockPredictionSarimax/yfinanceTeslaStockPredictionSarimax"
import DiscreteVsContinuousProbDistributions from "./each_blog_posts/Discrete-vs-Continuous-Probability-Distributions/DiscreteVsContinuousProbDistributions"
import DiffCalculusForGradientDescent from "./each_blog_posts/DiffCalculusForGradientDescent/DiffCalculusForGradientDescent"
import MultivariateCalculus from "./each_blog_posts/MultivariateCalculus/MultivariateCalculus"
import BiasVarianceTradeoff from "./each_blog_posts/BiasVarianceTradeoff/BiasVarianceTradeoff"
import VectorizingGradientDescentMultivariateLinearRegression from "./each_blog_posts/VectorizingGradientDescentMultivariateLinearRegression/VectorizingGradientDescentMultivariateLinearRegression"
import PrimeFactorWhyCheckUptoSquareRoot from "./each_blog_posts/PrimeFactorWhyCheckUptoSquareRoot/PrimeFactorWhyCheckUptoSquareRoot"
import AxesAndDimensionsInNumpy from "./each_blog_posts/AxesAndDimensionsInNumpy/AxesAndDimensionsInNumpy"
import DataRepresentationsForNeuralNetworksTensorVectorScaler from "./each_blog_posts/DataRepresentationsForNeuralNetworksTensorVectorScaler/DataRepresentationsForNeuralNetworksTensorVectorScaler"
import EuclideanDistanceAndNormalizationOfVectors from "./each_blog_posts/EuclideanDistanceAndNormalizationOfVectors/EuclideanDistanceAndNormalizationOfVectors"
import LocationSensitiveHashingCosineSimilarity from "./each_blog_posts/LocationSensitiveHashingCosineSimilarity/LocationSensitiveHashingCosineSimilarity"
import WhyLogTransformation from "./each_blog_posts/WhyLogTransformation/WhyLogTransformation"
import WhyF1Score from "./each_blog_posts/WhyF1Score/WhyF1Score"
import BatchNormalization from "./each_blog_posts/BatchNormalization/BatchNormalization"
import MatrixAlgebra from "./each_blog_posts/MatrixAlgebra/MatrixAlgebra"
import ConstantQTransformGravWaves from "./each_blog_posts/ConstantQTransformGravWaves/ConstantQTransformGravWaves"
import NNBackpropagationChainRule from "./each_blog_posts/NNBackpropagationChainRule/NNBackpropagationChainRule"
import KFoldCV from "./each_blog_posts/KFoldCV/KFoldCV"
import BootstrapSamplingInRandomForests from "./each_blog_posts/BootstrapSamplingInRandomForests/BootstrapSamplingInRandomForests"
import PlattScalingFromScratch from "./each_blog_posts/PlattScalingFromScratch/PlattScalingFromScratch"
import TFIDFFromScratch from "./each_blog_posts/TFIDFFromScratch/TFIDFFromScratch"
import TFIDFWithSklearn from "./each_blog_posts/TFIDFWithSklearn/TFIDFWithSklearn"
import DimensionalityReductionPCAMNIST from "./each_blog_posts/DimensionalityReductionPCAMNIST/DimensionalityReductionPCAMNIST"
import DimesionalityReductionTSNEMNIST from "./each_blog_posts/DimesionalityReductionTSNEMNIST/DimesionalityReductionTSNEMNIST"
import MultiArmedBanditSantaCompetition from "./each_blog_posts/MultiArmedBanditSantaCompetition/MultiArmedBanditSantaCompetition"
import PythonCommonChallenges from "./each_blog_posts/PythonCommonChallenges/PythonCommonChallenges"
import LogisticRegressionFromScratch from "./each_blog_posts/LogisticRegressionFromScratch/LogisticRegressionFromScratch"
import PytorchSnippets from "./each_blog_posts/PytorchSnippets/PytorchSnippets"
import KaggleHousePricesLinearRegression from "./each_blog_posts/KaggleHousePricesLinearRegression/KaggleHousePricesLinearRegression"
import HabermansCancerDatasetKaggleEDA from "./each_blog_posts/HabermansCancerDatasetKaggleEDA/HabermansCancerDatasetKaggleEDA"
import DCGANFromScratchTensorflow from "./each_blog_posts/DCGANFromScratchTensorflow/DCGANFromScratchTensorflow"
import DecisionFunctionSVMRBFKernelFromScratch from "./each_blog_posts/DecisionFunctionSVMRBFKernelFromScratch/DecisionFunctionSVMRBFKernelFromScratch"
import DecisionTreeDonorsChoose from "./each_blog_posts/DecisionTreeDonorsChoose/DecisionTreeDonorsChoose"
import FeatureEngNaiveBayesBoWDonorChoose from "./each_blog_posts/FeatureEngNaiveBayesBoWDonorChoose/FeatureEngNaiveBayesBoWDonorChoose"
import Harris_Corner_Detection from "./each_blog_posts/Harris_Corner_Detection/Harris_Corner_Detection"
import ShiTomasiCornerDetection from "./each_blog_posts/ShiTomasiCornerDetection/ShiTomasiCornerDetection"
import PerformanceMatrixFromScratchWithoutSklearn from "./each_blog_posts/PerformanceMatrixFromScratchWithoutSklearn/PerformanceMatrixFromScratchWithoutSklearn"
import MLAlgoFromScratchAll from "./each_blog_posts/MLAlgoFromScratchAll/MLAlgoFromScratchAll"
import ImageMatchingPanorama from "./each_blog_posts/ImageMatchingPanorama/ImageMatchingPanorama"
import NumpyUsefulTechniques from "./each_blog_posts/NumpyUsefulTechniques/NumpyUsefulTechniques"
import TFFundamentals45Mints from "./each_blog_posts/TFFundamentals45Mints/TFFundamentals45Mints"
import DeepNeuralNetworkwithPyTorchFMNISTDataset from "./each_blog_posts/DeepNeuralNetworkwithPyTorchFMNISTDataset/DeepNeuralNetworkwithPyTorchFMNISTDataset"
import DCGANFilterSizeInputShape from "./each_blog_posts/DCGANFilterSizeInputShape/DCGANFilterSizeInputShape"

const posts = [
  {
    title:
      "DCGAN - Generator Function - Understanding Filter Size and Input Shape",
    id: 58,
    date: 1643490555,
    src: `${process.env.PUBLIC_URL}/images/dcgan.jpg`,
    snippet:
      "Reason for using 4 * 4 * 515 Shape for the Input Dense Layer in the Generator Function.",
    content: <DCGANFilterSizeInputShape />,
  },
  {
    title: "Deep Neural Network - Base Pytorch Model - FMNIST Dataset",
    id: 57,
    date: 1642798092,
    src: `${process.env.PUBLIC_URL}/images/Pytorch.png`,
    snippet:
      "PyTorch Implementation of Classification on Fashion-MNIST dataset which consists of a training set of 60,000 images and test set of 10,000 images",
    content: <DeepNeuralNetworkwithPyTorchFMNISTDataset />,
  },
  {
    title: "Learn Tensorflow Fundamentals in 45 Mints",
    id: 56,
    date: 1640829450,
    src: `${process.env.PUBLIC_URL}/images/tf.jpg`,
    snippet: "A number of tips and techniques useful for daily usage",
    content: <TFFundamentals45Mints />,
  },
  {
    title: "Numpy Useful Techniques",
    id: 55,
    date: 1640829450,
    src: `${process.env.PUBLIC_URL}/images/numpy.jpg`,
    snippet: "A number of tips and techniques useful for daily usage",
    content: <NumpyUsefulTechniques />,
  },
  {
    title:
      "Image Matching & Stitching with Image Descriptors and Creating Panoramic Scene",
    id: 54,
    date: 1640656650,
    src: `${process.env.PUBLIC_URL}/images/panorama.jpg`,
    snippet:
      "Given a pair of images I want to stitch them to create a panoramic scene. ",
    content: <ImageMatchingPanorama />,
  },
  {
    title: "ML-Algorithms from scratch in pure python without using sklearn",
    id: 53,
    date: 1640311050,
    src: `${process.env.PUBLIC_URL}/images/scratch.jpg`,
    snippet:
      "TFIDF, GridSearchCV, RandomSearchCV, Decision Function of SVM, RBF Kernel, Platt Scaling to find P(Y==1|X) and SGD Classifier with Logloss and L2 regularization",
    content: <MLAlgoFromScratchAll />,
  },
  {
    title: "Computing performance metrics in Pure Python without scikit-learn",
    id: 52,
    date: 1640138250,
    src: `${process.env.PUBLIC_URL}/images/performance.jpg`,
    snippet:
      "Here I will compute Confusion Matrix, F1 Score, AUC Score without using scikit-learn",
    content: <PerformanceMatrixFromScratchWithoutSklearn />,
  },
  {
    title:
      "Understanding Shi-Tomasi Corner Detection Algorithm with OpenCV Python",
    id: 51,
    date: 1640051850,
    src: `${process.env.PUBLIC_URL}/images/si.jpg`,
    snippet:
      "Shi-Tomasi Corner Detection is an improved version of the Harris Corner Detection Algorithm.",
    content: <ShiTomasiCornerDetection />,
  },
  {
    title: "Understanding Harris Corner Detection Algorithm with OpenCV Python",
    id: 50,
    date: 1640051850,
    src: `${process.env.PUBLIC_URL}/images/harriscorner.jpg`,
    snippet:
      "Harris Corner Detection uses a score function to evaluate whether a point is a corner or not. First it computes the horizontal and vertical derivatives (edges) of an image,",
    content: <Harris_Corner_Detection />,
  },
  {
    title:
      "Feature-Engineering - Naive-Bayes with Bag-of-Words on Donor Choose Dataset",
    id: 49,
    date: 1639957499,
    src: `${process.env.PUBLIC_URL}/images/nb.jpg`,
    snippet:
      "Understanding Naive Bayes Mathematically and applying on Donor Choose Dataset",
    content: <FeatureEngNaiveBayesBoWDonorChoose />,
  },
  {
    title: "Decision Tree on Donors Choose Dataset Kaggle",
    id: 48,
    date: 1639784699,
    src: `${process.env.PUBLIC_URL}/images/dt.jpg`,
    snippet:
      "In this post, I will implement Decision Tree Algorithm on Donor Choose Dataset",
    content: <DecisionTreeDonorsChoose />,
  },
  {
    title:
      "Decision Function of SVM (Support Vector Machine) RBF Kernel - Building From Scratch",
    id: 47,
    date: 1639525499,
    src: `${process.env.PUBLIC_URL}/images/svm.jpg`,
    snippet:
      "Decision function is a method present in classifier{ SVC, Logistic Regression } class of sklearn machine learning framework. This method basically returns a Numpy array, In which each element represents whether a predicted sample for x_test by the classifier lies to the right or left side of the Hyperplane and also how far from the HyperPlane.",
    content: <DecisionFunctionSVMRBFKernelFromScratch />,
  },
  {
    title:
      "DCGAN from Scratch with Tensorflow Keras — Create Fake Images from CELEB-A Dataset",
    id: 46,
    date: 1639439099,
    src: `${process.env.PUBLIC_URL}/images/dcgan.jpg`,
    snippet:
      "A DCGAN (Deep Convolutional Generative Adversarial Network) is a direct extension of the GAN.",
    content: <DCGANFromScratchTensorflow />,
  },
  {
    title: "Kaggle Haberman's Survival Data Set - Exploratory Data Analysis",
    id: 45,
    date: 1639352699,
    src: `${process.env.PUBLIC_URL}/images/haberman.jpg`,
    snippet:
      "The Haberman's survival dataset covers cases from a study by University of Chicago's Billings Hospital done between 1958 and 1970 on the subject of patients-survival who had undergone surgery for breast cancer.",
    content: <HabermansCancerDatasetKaggleEDA />,
  },
  {
    title:
      "Kaggle House Prices Prediction Competition - Mathematics of Linear Regression",
    id: 44,
    date: 1639266299,
    src: `${process.env.PUBLIC_URL}/images/house.jpg`,
    snippet:
      "This blog on Linear Regression is about understanding mathematically the concept of Gradient Descent function and also some EDA.",
    content: <KaggleHousePricesLinearRegression />,
  },
  {
    title: "Common Pytorch Snippets",
    id: 43,
    date: 1639179899,
    src: `${process.env.PUBLIC_URL}/images/pytorch.jpg`,
    snippet:
      "In this notebook I will go over some regular snippets and techniques of it.",
    content: <PytorchSnippets />,
  },
  {
    title:
      "Understanding in Details the Mathematics of Logistic Regression and implementing From Scratch with pure Python",
    id: 42,
    date: 1639093499,
    src: `${process.env.PUBLIC_URL}/images/logistic.jpg`,
    snippet:
      "Logistic regression is a probabilistic classifier similar to the Naïve Bayes",
    content: <LogisticRegressionFromScratch />,
  },
  {
    title: "Most Common Python Challenges asked in Data Science Interview",
    id: 41,
    date: 1639007099,
    src: `${process.env.PUBLIC_URL}/images/python.jpg`,
    snippet:
      "In this Notebook I shall cover the following most common Python challenges for Data Science Interviews.",
    content: <PythonCommonChallenges />,
  },
  {
    title: "Multi ArmedBandit - Kaggle Santa Competition",
    id: 40,
    date: 1638661499,
    src: `${process.env.PUBLIC_URL}/images/multiarmed.jpg`,
    snippet:
      "Multi-armed bandit problems are some of the simplest reinforcement learning (RL) problems to solve. ",
    content: <MultiArmedBanditSantaCompetition />,
  },
  {
    title: "Dimensionality Reduction with t-SNE",
    id: 39,
    date: 1638488699,
    src: `${process.env.PUBLIC_URL}/images/tsne.jpg`,
    snippet:
      "In this post I will talk about Dimensionality Reduction with t-SNE (t-Distributed Stochastic Neighbor Embedding) using the famous **Digit Recognizer Dataset** (also known as MNIST data)",
    content: <DimesionalityReductionTSNEMNIST />,
  },
  {
    title: "Dimensionality Reduction with PCA",
    id: 38,
    date: 1638488699,
    src: `${process.env.PUBLIC_URL}/images/PCA.jpg`,
    snippet:
      "In this post I will be using Kaggle's famous **Digit Recognizer Dataset** (also known as MNIST data) to implement Dimensionality Reduction with PCA (Principle Component Analysis).",
    content: <DimensionalityReductionPCAMNIST />,
  },
  {
    title: "TF-IDF Model and its implementation with Scikit-learn",
    id: 37,
    date: 1638237450,
    src: `${process.env.PUBLIC_URL}/images/tfidf2.jpg`,
    snippet:
      "In this post, I shall go over TF-IDF Model and its implementation with Scikit-learn.",
    content: <TFIDFWithSklearn />,
  },
  {
    title: "TFIDF from scratch in pure python without using sklearn",
    id: 36,
    date: 1638237450,
    src: `${process.env.PUBLIC_URL}/images/tfidf.jpg`,
    snippet:
      "Here I shall implementing TFIDF from scratch in pure python without using sklearn or any other similar packages",
    content: <TFIDFFromScratch />,
  },
  {
    title: "Why Platt Scaling and implementation from scratch",
    id: 35,
    date: 1638064650,
    src: `${process.env.PUBLIC_URL}/images/platt.jpeg`,
    snippet:
      "Platt Scaling (PS) is probably the most prevailing parametric calibration method. It aims to train a sigmoid function to map the original outputs from a classifier to calibrated probabilities.",
    content: <PlattScalingFromScratch />,
  },
  {
    title:
      "Why Bootstrapping is useful and Implementation of Bootstrap Sampling in Random Forests",
    id: 34,
    date: 1637891850,
    src: `${process.env.PUBLIC_URL}/images/bootstrap.jpg`,
    snippet:
      "Bootstrapping resamples the original dataset with replacement many thousands of times to create simulated datasets. This process involves drawing random samples from the original dataset.",
    content: <BootstrapSamplingInRandomForests />,
  },
  {
    title:
      "What K-Fold Cross Validation really is in Machine Learning in simple terms",
    id: 33,
    date: 1637632650,
    src: `${process.env.PUBLIC_URL}/images/kfold.jpg`,
    snippet:
      "k-fold cross-validation is one of the most popular strategies widely used by data scientists. It is a data partitioning strategy so that you can effectively use your dataset to build a more generalized model. ",
    content: <KFoldCV />,
  },
  {
    title: "Neural Network — Implementing Backpropagation using the Chain Rule",
    id: 32,
    date: 1637373450,
    src: `${process.env.PUBLIC_URL}/images/nn.jpg`,
    snippet:
      "In this post, I will go over the mathematical need and the derivation of Chain Rule in a Backpropagation process.",
    content: <NNBackpropagationChainRule />,
  },
  {
    title: "Constant-Q transform with Gravitational Waves Data",
    id: 31,
    date: 1637200650,
    src: `${process.env.PUBLIC_URL}/images/constantq.png`,
    snippet:
      "The constant-Q transform transforms a data series to the frequency domain. It is related to the Fourier transform.",
    content: <ConstantQTransformGravWaves />,
  },
  {
    title: "All the basic Matrix Algebra you will need in Data Science",
    id: 30,
    date: 1636941450,
    src: `${process.env.PUBLIC_URL}/images/MatrixAlgebra.jpeg`,
    snippet: "Most used Matrix Maths in a Nutshell",
    content: <MatrixAlgebra />,
  },
  {
    title: "Batch Normalization in Deep Learning",
    id: 29,
    date: 1636768650,
    src: `${process.env.PUBLIC_URL}/images/BatchNormalization.jpg`,
    snippet:
      "A batch normalization layer calculates the mean and standard deviation of each of its input channels across the batch and normalizes by subtracting the mean and dividing by the standard deviation.",
    content: <BatchNormalization />,
  },
  {
    title: "Why F1 Score uses Harmonic Mean and not an Arithmetic Mean",
    id: 28,
    date: 1636509450,
    src: `${process.env.PUBLIC_URL}/images/f1.jpg`,
    snippet:
      "The F1 score is the harmonic mean of precision and recall, taking both metrics into account",
    content: <WhyF1Score />,
  },
  {
    title:
      "Why we do log transformation of variables and interpretation of Logloss",
    id: 27,
    date: 1636336650,
    src: `${process.env.PUBLIC_URL}/images/LogTransformation.jpg`,
    snippet: "Original number = x and Log Transformed number x=log(x)",
    content: <WhyLogTransformation />,
  },
  {
    title: "Location-Sensitive-Hashing-for-cosine-similarity",
    id: 26,
    date: 1636163850,
    src: `${process.env.PUBLIC_URL}/images/LSH.jpg`,
    snippet: "What is cosine distance and cosine similarity",
    content: <LocationSensitiveHashingCosineSimilarity />,
  },
  {
    title: "Euclidean Distance and Normalization of a Vector",
    id: 25,
    date: 1635991050,
    src: `${process.env.PUBLIC_URL}/images/distance.jpg`,
    snippet:
      "Euclidean distance is the shortest distance between two points in an N-dimensional space also known as Euclidean space. ",
    content: <EuclideanDistanceAndNormalizationOfVectors />,
  },
  {
    title:
      "Data Representations For Neural Networks - Understanding Tensor, Vector and Scaler",
    id: 24,
    date: 1635991050,
    src: `${process.env.PUBLIC_URL}/images/TensorVectorScaler.jpg`,
    snippet:
      "At its core, a tensor is a container for data — almost always numerical data.",
    content: <DataRepresentationsForNeuralNetworksTensorVectorScaler />,
  },
  {
    title: "Axes and Dimensions in Numpy and Pandas Array",
    id: 23,
    date: 1635818250,
    src: `${process.env.PUBLIC_URL}/images/AxesAndDimensionsInNumpy.jpg`,
    snippet:
      "Understanding the shape and Dimension will be one of the most crucial thing in Machine Learning and Deep Learning Project. This blog makes it clear.",
    content: <AxesAndDimensionsInNumpy />,
  },
  {
    title:
      "Prime Factorization of a Number in Python and why we check upto the square root of the Number",
    id: 22,
    date: 1635731850,
    src: `${process.env.PUBLIC_URL}/images/PrimeFactors.jpeg`,
    snippet: "Understanding the Mathematical Reasoning",
    content: <PrimeFactorWhyCheckUptoSquareRoot />,
  },
  {
    title:
      "Vectorizing Gradient Descent — Multivariate Linear Regression and Python implementation",
    id: 21,
    date: 1635731850,
    src: `${process.env.PUBLIC_URL}/images/VectorizingGradientDescentMultivariateLinearRegression.jpg`,
    snippet:
      "Vectorized Gradient-Descent formulae for the Cost function of the for Matrix form of training-data Equations.",
    content: <VectorizingGradientDescentMultivariateLinearRegression />,
  },
  {
    title: "Understanding Bias-Variance Trade-off",
    id: 20,
    date: 1635127050,
    src: `${process.env.PUBLIC_URL}/images/BiasVarianceTradeoff.gif`,
    snippet:
      "In this post I shall discuss the concept around and the Mathematics behind the below formulation of Bias-Variance Tradeoff.",
    content: <BiasVarianceTradeoff />,
  },
  {
    title:
      "Fundamentals of Multivariate Calculus for DataScience and Machine Learning",
    id: 19,
    date: 1634608650,
    src: `${process.env.PUBLIC_URL}/images/MultivariateCalculus.jpeg`,
    snippet:
      "as soon as you need to implement multi-variate Linear Regression, you hit multivariate-calculus which is what you will have to use to derive the Gradient of a set of multi-variate Linear Equations i.e. Derivative of a Matrix.",
    content: <MultivariateCalculus />,
  },
  {
    title:
      "Concepts of Differential Calculus for understanding the derivation of Gradient Descent in Linear Regression",
    id: 18,
    date: 1634263050,
    src: `${process.env.PUBLIC_URL}/images/DiffCalcGradDescent.jpg`,
    snippet:
      "The most fundamental definition of Derivative can be stated as — derivative measures the steepness of the graph of a function at some particular point on the graph. ",
    content: <DiffCalculusForGradientDescent />,
  },
  {
    title:
      "Discrete vs Continuous Probability Distributions in context of Data Science",
    id: 17,
    date: 1633744650,
    src: `${process.env.PUBLIC_URL}/images/DiscreteContProb.jpg`,
    snippet: "Discrete and Continuos Random Variable and related Probabilities",
    content: <DiscreteVsContinuousProbDistributions />,
  },
  {
    title:
      "yfinance to get historical financials from free and Tesla Stock Prediction",
    id: 16,
    date: 1633485450,
    src: `${process.env.PUBLIC_URL}/images/yfinance.jpg`,
    snippet:
      "Using yfinance open source library access great amount of historical financial data for Free",
    content: <YfinanceTeslaStockPredictionSarimax />,
  },
  {
    title: "The mean, the median, and mode of a Random Variable",
    id: 15,
    date: 1633485450,
    src: `${process.env.PUBLIC_URL}/images/Mean.png`,
    snippet:
      "The mean is called a measure of central tendency because it tells us something about the center of a distribution, specifically its center.",
    content: <MeanMedianMode />,
  },
  {
    title: "BitCoin Price Prediction using Deep Learning (LSTM Model)",
    id: 14,
    date: 1633312650,
    src: `${process.env.PUBLIC_URL}/images/BTC_LSTM.jpg`,
    snippet:
      "Using the historical data, I will implement a recurrent neural netwok using LSTM (Long short-term memory) layers to predict the trend of cryptocurrency values in the future.",
    content: <BtcLSTM />,
  },
  {
    title: "Moving Averages with Bitcoin Historical Data",
    id: 13,
    date: 1632967050,
    src: `${process.env.PUBLIC_URL}/images/BtcEdaMovingAverage.jpg`,
    snippet:
      "Moving averages are one of the most often-cited data-parameter in the space of Stock market trading, technical analysis of market and is extremely useful for forecasting long-term trends.",
    content: <BtcEdaMovingAverage />,
  },
  {
    title: "Categorical Data Handling in Machine Learning Projects",
    id: 12,
    date: 1632880650,
    src: `${process.env.PUBLIC_URL}/images/CategoricalVariablesHandling.jpg`,
    snippet:
      "Typically, any data attribute which is categorical in nature represents discrete values which belong to a specific finite set of categories or classes.",
    content: <CategoricalVariablesHandling />,
  },
  {
    title:
      "Tensorflow-Keras Custom Layers and Fundamental Building Blocks For Training Model",
    id: 11,
    date: 1632621450,
    src: `${process.env.PUBLIC_URL}/images/TF_Custom_Layers.jpeg`,
    snippet:
      "In TensorFlow, we can define custom data augmentations(e.g. mixup, cut mix) as a custom layer using subclassing, which I will talk about in this blog",
    content: <TFCustomLayersFundamentalBuildingBlocksForTrainingModel />,
  },
  {
    title:
      "Tensorflow Mixed Precision Training — CIFAR10 dataset Implementation",
    id: 10,
    date: 1632362250,
    src: `${process.env.PUBLIC_URL}/images/TFMixedPrecision.jpeg`,
    snippet:
      "Mixed precision training is a technique used in training a large neural network where the model’s parameter are stored in different datatype precision (FP16 vs FP32 vs FP64). It offers significant performance and computational boost by training large neural networks in lower precision formats. ",
    content: <TensorflowMixedPrecisionTrainingCIFAR10 />,
  },
  {
    title:
      "Predict Transaction Value of a Bank’s Potential Customers — Kaggle Santander Competition",
    id: 9,
    date: 1632189450,
    src: `${process.env.PUBLIC_URL}/images/santander.jpeg`,
    snippet:
      "For this Project — I applied LightGBM + XGBoost + CatBoost Ensemble to achieve a Top 11% score in the Kaggle Competition ( Santander Value…",
    content: <SantanderValuePredictionKaggle />,
  },
  {
    title:
      "Python Image Processing - Grayscale Conversion and Brightness Increase from Scratch",
    id: 8,
    date: 1631930250,
    src: `${process.env.PUBLIC_URL}/images/grayscale_brightness.jpeg`,
    snippet:
      "Understanding from scratch how to convert an image to Grayscale and increase the Brightness of an image, working at the pixel level and some related fundamentals of image processing with Python",
    content: <GrayScaleBrightness />,
  },
  {
    title:
      "Gravitational Waves Kaggle Competition Part-2 - EDA and Baseline TensorFlow / Keras Model",
    id: 7,
    date: 1631498250,
    src: `${process.env.PUBLIC_URL}/images/gravitational2.png`,
    snippet:
      "In this Kaggle Competition, data-science helping to find Gravitational Waves by building models to filter out noises from data-streams",
    content: <GravitationalWaves2 />,
  },
  {
    title: "Gravitational Waves Detection - Kaggle Competition Part-1",
    id: 6,
    date: 1631411850,
    src: `${process.env.PUBLIC_URL}/images/gravitational1.png`,
    snippet:
      "The great Kaggle Competition for G2Net Gravitational Wave Detection. Here, I shall go through the fundamental introduction on Gravitational waves, and some related concepts required for this competition.",
    content: <GWDetectionPage />,
  },
  {
    title: "Microsoft Malware Detection Kaggle Challenge",
    id: 5,
    date: 1631066250,
    src: `${process.env.PUBLIC_URL}/images/msmalware1.png`,
    snippet:
      "Here I apply a large number of Feature Engineering to extract features from the 500GB dataset of Microsoft Malware Classification Kaggle Competiton and then apply XGBoost to achieve a LogLoss of 0.007.",
    content: <MSMalwareDetection />,
  },

  {
    title:
      "Implementing Stochastic Gradient Descent Classifier with Logloss and L2 regularization without sklearn",
    id: 4,
    date: 1630807050,
    src: `${process.env.PUBLIC_URL}/images/sgd-from-scratch.jpg`,
    snippet:
      "In SGD while selecting data points at each step to calculate the derivatives. SGD randomly picks one data point from the whole data set at each iteration to reduce the computations enormously",
    content: <SGDFromScratch />,
  },
  {
    title: "Implementing RandomSearchCV from scratch (without scikit-learn)",
    id: 3,
    date: 1630461450,
    src: `${process.env.PUBLIC_URL}/images/randomsearch.jpg`,
    snippet:
      "Random Search sets up a grid of hyperparameter values and selects random combinations to train the model and score.",
    content: <RandomSearchCVFromScratch />,
  },

  {
    title:
      "Implementing Custom GridSearchCV from scratch (without scikit-learn)",
    id: 2,
    date: 1630288650,
    src: `${process.env.PUBLIC_URL}/images/gridsearch.jpg`,
    snippet: "Implementing Custom GridSearchCV without scikit-learn.",
    content: <GridSearchCVFromScratch />,
  },
  {
    title: "Probability Problem-Solution series for Data Science -Part-1",
    id: 1,
    date: 1629770250,
    src: `${process.env.PUBLIC_URL}/images/prob1.jpg`,
    snippet:
      "A series solving some fundamental Probability Problems in the context of DataScience and Machine Learning",
    content: <ProbabilityProblemSolution1 />,
  },
]

export default posts
