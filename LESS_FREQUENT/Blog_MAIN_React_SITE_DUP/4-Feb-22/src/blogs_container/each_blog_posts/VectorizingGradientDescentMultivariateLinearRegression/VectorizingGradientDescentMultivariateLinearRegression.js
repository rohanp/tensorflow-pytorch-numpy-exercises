import React, { Component, Fragment } from "react"
import JupyterViewer from "react-jupyter-notebook"

import nb_blog from "./VectorizingGradientDescentMultivariateLinearRegression.json"
// Just manually save the .ipynb file as a .json file.

class VectorizingGradientDescentMultivariateLinearRegression extends Component {
  render() {
    return (
      <Fragment>
        <JupyterViewer
          rawIpynb={nb_blog}
          displaySource="show"
          displayOutput="show"
          codeBlockStyles={{ hljsStyle: "dark" }}
        />
      </Fragment>
    )
  }
}

export default VectorizingGradientDescentMultivariateLinearRegression
