import React, { Component, Fragment } from "react"
import JupyterViewer from "react-jupyter-notebook"

import nb_blog from "./EuclideanDistanceAndNormalizationOfVectors.json"
// Just manually save the .ipynb file as a .json file.

class EuclideanDistanceAndNormalizationOfVectors extends Component {
  render() {
    return (
      <Fragment>
        <JupyterViewer
          rawIpynb={nb_blog}
          displaySource="show"
          displayOutput="show"
          codeBlockStyles={{ hljsStyle: "dark" }}
        />
      </Fragment>
    )
  }
}

export default EuclideanDistanceAndNormalizationOfVectors
