import React, { Component, Fragment } from "react"
import YouTube from "react-youtube"

import nb_blog from "./EDA_CQT_Keras_Baseline.json"
// Just manually save the .ipynb file as a .json file.
// This is what is required by the recently update package
// https://github.com/Joeyonng/react-jupyter-notebook#usage

// import JupyterViewer from "react-jupyter-notebook"
// With the above Line I am importing the JupyterViewer from this recently updated package
// https://github.com/Joeyonng/react-jupyter-notebook

// BUT if I do NOT want to directly depend on the package
// I have to download the actual code of the package - i.e. all the files from this directory of the package.
// https://github.com/Joeyonng/react-jupyter-notebook/tree/master/src/lib
// and then import the relevant Component like so.
import JupyterViewer from "../../JupViewer_Modified_By_the_recent_package/JupyterViewer"

/* I should be able to do exact similar structure with this original package as well
https://github.com/ShivBhosale/React-Jupyter-Viewer/tree/master/ipynb-viewer
However I have NOT tried this with this original Repo */

class GravitationalWaves2 extends Component {
  render() {
    const opts = {
      height: "440",
      width: "810",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 0, // If 1 it will start playing automatically
      },
    }

    const videoId = "UeI4-kyuAwI"

    return (
      <Fragment>
        <YouTube videoId={videoId} opts={opts} onReady={this._onReady} />

        <JupyterViewer
          rawIpynb={nb_blog}
          displaySource="show"
          displayOutput="show"
          codeBlockStyles={{ hljsStyle: "dark" }}
        />

        <YouTube videoId={videoId} opts={opts} onReady={this._onReady} />
      </Fragment>
    )
  }
}

export default GravitationalWaves2
