import React, { Component, Fragment } from "react"
import JupyterViewer from "react-jupyter-notebook"
import YouTube from "react-youtube"

import nb_blog from "./ConstantQTransformGravWaves.json"
// Just manually save the .ipynb file as a .json file.

class ConstantQTransformGravWaves extends Component {
  render() {
    const opts = {
      height: "440",
      width: "810",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 0, // If 1 it will start playing automatically
      },
    }

    const videoId = "UeI4-kyuAwI"
    return (
      <Fragment>
        <YouTube videoId={videoId} opts={opts} onReady={this._onReady} />
        <JupyterViewer
          rawIpynb={nb_blog}
          displaySource="show"
          displayOutput="show"
          codeBlockStyles={{ hljsStyle: "dark" }}
        />
        <YouTube videoId={videoId} opts={opts} onReady={this._onReady} />
      </Fragment>
    )
  }
}

export default ConstantQTransformGravWaves
