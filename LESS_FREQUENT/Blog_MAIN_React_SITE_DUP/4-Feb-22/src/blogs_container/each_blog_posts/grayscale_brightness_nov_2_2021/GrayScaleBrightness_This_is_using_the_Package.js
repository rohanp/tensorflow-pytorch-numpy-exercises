import React, { Component, Fragment } from "react"
import YouTube from "react-youtube"
import JupyterViewer from "react-jupyter-notebook"
import nb_blog from "./GrayScale_Brightness.json" // Just manually save the .ipynb file as a .json file.

class GrayScaleBrightness extends Component {
  render() {
    const opts = {
      height: "440",
      width: "810",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
        autoplay: 0, // If 1 it will start playing automatically
      },
    }

    return (
      <Fragment>
        <YouTube videoId="I8kwtcVSEDk" opts={opts} onReady={this._onReady} />

        <JupyterViewer
          rawIpynb={nb_blog}
          displaySource="show"
          displayOutput="show"
          codeBlockStyles={{ hljsStyle: "dark" }}
        />

        <YouTube videoId="I8kwtcVSEDk" opts={opts} onReady={this._onReady} />
      </Fragment>
    )
  }
}

export default GrayScaleBrightness
