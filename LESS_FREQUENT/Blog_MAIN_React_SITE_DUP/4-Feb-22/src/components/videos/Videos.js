import React, { Component, Fragment } from "react"
import ReactPlayer from "react-player"
// import YouTube from "react-youtube"
// import PropTypes from "prop-types"
// import classNames from "classnames"
import { Grid } from "@material-ui/core"
// import { Grid, Box, isWidthUp, withWidth, withStyles } from "@material-ui/core"
import "./Videos.css"
// import { Paper } from "@mui/material"

const video_lists = [
  "https://youtu.be/Z_JYzPX99Fk?list=PLxqBkZuBynVR0Tw4dRwhpBcVBwePdLv8y",
  "https://youtu.be/_3rIOGR-jh0?list=PLxqBkZuBynVS8mDTc8ZGermXiS-32pR2y",
  "https://youtu.be/-MVHZvB5GhU?list=PLxqBkZuBynVS8mDTc8ZGermXiS-32pR2y",
  "https://youtu.be/8YJRtyx10vw?list=PLxqBkZuBynVRMORlFw95iNTp9aZzmmz4Y",
  "https://youtu.be/h4Hp7mrM2zg?list=PLxqBkZuBynVRMORlFw95iNTp9aZzmmz4Y",
  "https://youtu.be/UeI4-kyuAwI?list=PLxqBkZuBynVS8mDTc8ZGermXiS-32pR2y",
  "https://youtu.be/csQj1e6Oj38?list=PLxqBkZuBynVS8mDTc8ZGermXiS-32pR2y",
  "https://youtu.be/QQkrIlISzP0?list=PLxqBkZuBynVRX6QExfPyzRGj5Ap_zmcAJ",
]

class Videos extends Component {
  render() {
    // const opts = {
    //   height: "300",
    //   width: "410",
    //   playerVars: {
    //     // https://developers.google.com/youtube/player_parameters
    //     autoplay: 0, // If 1 it will start playing automatically
    //   },
    // }
    return (
      <Fragment>
        {/* <YouTube videoId="QI0qjDfMtAw" opts={opts} /> */}
        <div className="playerWrapper">
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            {/* {Array.from(Array(6)).map((_, index) => ( */}
            {video_lists.map((video, index) => (
              <Grid item xs={1} sm={3} md={3} key={index}>
                {/* {console.log(video)} */}
                {/* <YouTube videoId="QI0qjDfMtAw" opts={opts} /> */}

                <div className="eachPlayerWrapper">
                  <ReactPlayer
                    className="react-player"
                    url={video}
                    width="100%"
                    height="100%"
                  />
                </div>
              </Grid>
            ))}
          </Grid>
        </div>
      </Fragment>
    )
  }
}

export default Videos

// return <YouTube videoId="QI0qjDfMtAw" opts={opts} onReady={this._onReady} />
