## My Personal and Blog website (Currently Work in Progress). [Live HERE](https://rohan-paul-ai.netlify.app/)

[![IMAGE ALT TEXT](https://imgur.com/F1NULLg.png)](https://rohan-paul-ai.netlify.app/)

---

## To build the site locally (This is MUST Before doing `netlify deploy` as I have stopped auto-build in Netlify so Netlify will ONLY build from my local /build folder)

```
npm run build
```

This will create a folder named `/build` with the compiled project inside of it.

---

## Regular Netlify Manual Deployment - It will deploy from the local /build directory

```
npm run build

netlify deploy

netlify deploy --prod
```

OR

```
npm run build && netlify deploy && netlify deploy --prod
```

---

## More details on Manual deploys

I have DISABLED the automatic build - Which would have directly built from Github after each of my `git push` to the Main branch.

Disabling is VERY IMPORTANT - ELSE MY 300 Minutes BUILD allocation will be exhausted in about 15 days, Because Netlify will build for each git push to Main Branch

This option can be found under **site Settings > Build & deploy > Continuous deployment > Build settings > Edit > Stop Build.**

![](public/images/2021-12-06-14-31-42.png)

And after this step, I can NOT do a manual 'Trigger Deploy' as that option will be grayed-out.

![](public/images/2021-12-06-14-32-56.png)

It’s also possible to deploy a site manually, without continuous deployment.

**This method uploads files directly from your local project directory to your site on Netlify**.

### And so before running `netlify deploy` I have to MUST run `npm run build`

**If builds are stopped, manual deploys are the only way you can update your site.**

A common use case for this command is when you’re using a separate Continuous Integration (CI) tool, deploying prebuilt files to Netlify at the end of the CI tool tasks.

First build the project locally React app (i.e. `npm run build`)
the Netlify CLI tool doesn’t use a local cache. If something is being cached, it is being cached by your local build tools and not the CLI tool.

**To get started with manual deploys, run the following command from your project directory:**

```
npm run build

netlify deploy

netlify deploy --prod
```

or I could also mention the directory which need to be published/deployed after build

By default I checked that, netlify takes the `build` directory at project root

```
netlify deploy --dir=build

netlify deploy --prod --dir=build
```

---

## Note about configuring Netlify Deployment

### 1. Prevent Netlify from treating warnings as errors because process.env.CI = true?

```
CI=false npm run build
```

### 2. Also for the first time deployment, dont include anything for the "homepage" key in package.json. i.e. keep that line completely deleted or may be empty string.

### IMPORTANT-REACT ROUTER ISSUE - Avoid Page Not Found Error in any individual pages inside the site

Known ISSUE For React Router SPA App in Netlify - When visiting any pages without first visiting the home page in a Netlify Deployed site - You will get 'Page Not Found' Error.

**Reason** - React Router handles routing on the client-side (browser) so when you visit the non-root page (e.g. https://yoursite.netlify.com/login), Netlify (server-side) does not know how to handle the route.

### Solution

To fix the issue,

- Create a file named `_redirects` to the root of our site which is the `/public` folder at the very root of your React App. And include inside that just this line and NOTHING else.

```
/* /index.html 200
```

### [Official Guide on this is here](https://docs.netlify.com/routing/redirects/rewrites-proxies/#history-pushstate-and-single-page-apps)

This will effectively serve the `index.html` first to Netlify Server - instead of giving a 404 no matter what URL the browser requests. And once internally the `index.html` is served all the React Router client things has come to Netlify and cached and then right away that particular page will be served at the very first time.
