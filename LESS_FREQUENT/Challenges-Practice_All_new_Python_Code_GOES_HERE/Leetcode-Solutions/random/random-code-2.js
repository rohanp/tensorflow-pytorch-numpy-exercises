const reverseNum = x => {
    let result = 0;
    
    if (x < 0) {
        // For this case, slice the string after the first "-" sign. Else I will get a NaN
        result = -(('' + x).slice(1).split('').reverse().join(''));
    } else {
        result = +(('' + x).split('').reverse().join(''));
    }

    // In split() and join() I have to pass the delimiter of an empty string as '' - Else tit will be an error.

    return (result < -2147483648 || result > 2147483648)  ? 0 : result
}

console.log(reverseNum( 1534236469 )) // => 0