{  
  "results":[  
    {  
      "gender":"female",
      "name":{  
        "title":"mrs",
        "first":"sofie",
        "last":"nielsen"
      },
      "location":{  
        "street":"2047 skovgade",
        "city":"aarhus n",
        "state":"danmark",
        "postcode":64839
      },
      "email":"sofie.nielsen@example.com",
      "login":{  
        "username":"smallleopard623",
        "password":"sanders",
        "salt":"MHEGpD47",
        "md5":"8646797800566384010613b0d56c2c1c",
        "sha1":"e328d8e580bd5bf9104ab802bfe4899ea80be52b",
        "sha256":"5e1cf8fe8fc57dace8cbaaf122f1ac78113780af69ef95e5c6c6b57636ed3264"
      },
      "dob":"1952-08-12 02:47:05",
      "registered":"2015-12-08 09:10:51",
      "phone":"42371394",
      "cell":"41755659",
      "id":{  
        "name":"CPR",
        "value":"538349-3768"
      },
      "picture":{  
        "large":"https://randomuser.me/api/portraits/women/29.jpg",
        "medium":"https://randomuser.me/api/portraits/med/women/29.jpg",
        "thumbnail":"https://randomuser.me/api/portraits/thumb/women/29.jpg"
      },
      "nat":"DK"
    }
  ],
  "info":{  
    "seed":"fecdeabc77280730",
    "results":1,
    "page":1,
    "version":"1.1"
  }
}