n_th_fibonacci = n => {
    let [a, b] = [0, 1];
    let fibSeries = [1];

    while (--n) {
        [a, b] = [b, b + a ]
        fibSeries.push(b)
    }
    return fibSeries;
}

console.log(n_th_fibonacci(10)) // => 55