## How Torchvision dataset ImageFolder works ?

ImageFolder is a generic data loader class in torchvision that helps you load your own image dataset.

As the first requirement, the input data should be in a different folder per label for the default PyTorch ImageFolder to load it correctly. Like - train/dog, - train/cat, - test/dog, - test/cat and then passing the train and the test folder to the train and test ImageFolder respectively.


So if the directory is given as explained above, we only need to initialize an instance of ImageFolder class with the root. The instance will return the randomly selected and transformed batch with corresponding labels.

 ## Transform

In order to augment the dataset, we apply various transformation techniques. These include the crop, resize, rotation, translation, flip and so on. In the pyTorch, those operations are defined in the ‘torchvision.transforms’

Generally, transformation does help the model to prevent the overfitting issue. However sometime in cases, where there is no  overfitting issue in the model, after applying a lot of transformation, the training loss and validation loss may actually worsen.


#### RandomHorizontalFlip

This transformation will flip the image horizontally (random) with a given probability. You can set this probability through the parameter ‘p’. The default value of p is 0.5.

#### RandomRotation
This transformation rotates the image randomly by an angle. The angle in degrees can be provided as input to that parameter “degrees”.

```py

transform = transforms.Compose([transforms.RandomRotation(degrees=180)])
tensor_img = transform(image)

```

#### RandomVerticalFlip(p):

This function will flip the given image vertically in random with a given probability.

#### RandomPerspective(distortion_scale, p):

This unction will perform perspective transformation of the given image randomly given a probability. It reduces the effect of perspective for model learning by distorting whole the image.

![Imgur](https://imgur.com/lFO4zCs.png)


#### ColorJitter(brightness, contrast, saturation, hue):

I can randomly change the brightness, contrast and saturation of an image


#### CenterCrop

Crops the given image at the center. If the image is torch Tensor, it is expected to have […, H, W] shape, where … means an arbitrary number of leading dimensions. If image size is smaller than output size along any edge, image is padded with 0 and then center cropped.

**transforms.ToTensor()** convert data array to Pytorch tensor. Advantage include easy to use in CUDA, GPU training.

------------------------


### Why Pytorch officially use mean=[0.485, 0.456, 0.406] and std=[0.229, 0.224, 0.225] to normalize images ?

First the normalization works as below to modify the input training dataset's channel values

#### input[channel] = (input[channel] - mean[channel]) / std[channel]

The mean and standard deviation values are to be taken from the training dataset.

Per [Doc](https://pytorch.org/vision/stable/models.html) -

All pre-trained models expect input images normalized in the same way, i.e. mini-batches of 3-channel RGB images of shape (3 x H x W), where H and W are expected to be at least 224. The images have to be loaded in to a range of [0, 1] and then normalized using mean = [0.485, 0.456, 0.406] and std = [0.229, 0.224, 0.225].

This is because, the above values for Normalization() is calculated based on the ImageNet Dataset.


Using the mean and std of Imagenet is a common practice. They are calculated based on millions of images. If you want to train from scratch on your own dataset, you can calculate the new mean and std. Otherwise, using the Imagenet pretrianed model with its own mean and std is recommended.

Further on this point, whether or not to use ImageNet's mean and stddev depends on your data. Assuming your data are ordinary photos of "natural scenes"† (people, buildings, animals, varied lighting/angles/backgrounds, etc.), and assuming your dataset is biased in the same way ImageNet is (in terms of class balance), then it's ok to normalize with ImageNet's scene statistics. If the photos are "special" somehow (color filtered, contrast adjusted, uncommon lighting, etc.) or an "un-natural subject" (medical images, satellite imagery, hand drawings, etc.) then you need to experiment with the respective training dataset's own mean and standard deviation values.

Example code for using values of (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)

```py
transform=transforms.Compose([transforms.ToTensor(),
                              transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                             ])
trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transform)
```

And in case you are using new Mean and Variance from your own training dataset - then don't forget to scale the validation and test set using the mean and variance of the training set, rather than their own mean and variance. Otherwise you introduce a domain shift.

Domain shift refers to the difference in the data distribution of two datasets, normally between the training set and the test set.

---------------------------------------------------


### DataLoader

DataLoader of `torch.utils.data` package is what actually returns the batch given the transformations and data directory that we set with the above Transform and ImageFolder class.

For a regular case of using DataLoader() function, I could just pass it the dataset returned from the ImageFolder() function like below.

```py

# ImageFolder with root directory and defined transformation methods for batch as well as data augmentation
data = torchvision.datasets.ImageFolder(root=data_dir, transform=transform['train'] if train else 'test')
data_loader = torch.utils.data.DataLoader(dataset=data, batch_size=batch_size, shuffle=True, num_workers=4)

```

------------------------------------------------------


## What does `torch.backends.cudnn.benchmark = True` does

It enables benchmark mode in cudnn, and so allowing you to enable the inbuilt cudnn auto-tuner to find the best algorithm to use for your hardware.

Benchmark mode is good whenever your input sizes for your network do not vary. This way, cudnn will look for the optimal set of algorithms for that particular configuration (which takes some time). This usually leads to faster runtime.

But if your input sizes changes at each iteration, then cudnn will benchmark every time a new size appears, possibly leading to worse runtime performances. Because, each new input shape will rerun `cudnnFind` to find the fastest kernel for this shape (for all layers with a new input shape) and will add these kernels to a cache.

If your input always have the same size, you should enable it all the time.

However, if your model changes: for instance, if you have layers that are only "activated" when certain conditions are met, or you have layers inside a loop that can be iterated a different number of times, then setting torch.backends.cudnn.benchmark = True might stall your execution.