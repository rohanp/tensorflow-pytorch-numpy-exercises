https://datascience.stackexchange.com/questions/26382/what-is-the-shape-of-conv3d-and-conv3d-transpose

## What is the shape of conv3d and conv3d_transpose?

It is an order 5 tensor, and the dimensions are: BatchSize × Depth × Height × Width × Channels


===========================================================================

## What is the difference between 2d vs 3d convolutions?

https://forums.fast.ai/t/what-is-the-difference-between-2d-vs-3d-convolutions/52495

- 2d convolutions map (N,C_in,H,W) -> (N,C_out,H_out,W_out)

- 3d convolutions map (N,C_in,D,H,W) -> (N,C_out,D_out,H_out,W_out)


Which make sense to me. However, what I find confusing is that I would have expected images to be considered 3D tensors but we apply 2D convolutions to them. Why is that? Why is the Channel tensor not part of the “definitionality of the images”?

Ans

One way to think about it is via the „movement“ of the convolutional filter. In an image the conv filter gets moved horizontally and vertically (so in x and y) across the image, so in 2 dimensions, hence a Conv2D, no matter whether it is a greyscale image (1 channel), color image (3 channels) or medical or sattelite image with 4 or more channels. The only thing that changes is that the conv2D filter needs to have a matching amount of in-channels in the third dimension.

In a 3D conv, the 3rd dimension is the depth and the conv filter gets moved along that dimension too, so for example a 3x3x3 filter also gets moved in x, y and z across the volume. The input in that case has more than 3 dimensions, for example x,y,z and reflectivity for some lidar data.



===========================================================================

## When to use Conv3D instead of Conv2D

https://stackoverflow.com/a/58900084/1902852

To understand this concept, first revisit the main advantages of CNN

One of the main benefits of convolutional layers over fully connected 2D layers is that the the weights are local to a 2D area and shared over all 2D positions, i.e. a filter. This means that a discriminatory pattern in the image is learned once even if it occurs multiple times or in different positions. I.e. it is somewhat invariant to translation.

For a 3D signal you need to work out if you need the filter output to be invariant to depth, that is, the discriminatory features could occur at any or more than one depth in the image, or if the depth position of features is relatively fixed. The former would need 3D convolutions, the latter you could get away with 2D convolutions with lots of channels.

For example, say you had a 3D scan of someone's lungs and you are trying to classify if there is a tumour or not. For this you would need 3D convolution because the combination of filters that represents "tumour" needs to be invariant to both the X, Y and Z positions of that tumour. If you used a 2D convolution in this case, the training set must have examples of the tumour at all different Z positions, otherwise the network will be very sensitive to the Z position.


https://forums.fast.ai/t/are-there-any-successful-vision-models-that-use-3d-convolutions/52503

3D CNNs are used at least for volumetric data and hyperspectral image classification. There is no advantage to use 3D convolutions for imagenet or cifar 10, because they only have 3 channels . You can think of it this way: both approaches find interesting features, but when 2d cnns find those in only two dimensions (x and y axis), 3d filters account also the z-axis (time, volume, spectral dimension).

Some examples where 3D CNN:s are used:

V-Net for volumetric image segmentation https://arxiv.org/pdf/1606.04797.pdf 26
Smoke detection on Video Sequences https://link.springer.com/article/10.1007/s10694-019-00832-w

===========================================================================

## Basic structure

https://datascience.stackexchange.com/a/51474/101199

**Conv1D** is used for input signals which are similar to the voice. By employing them you can find patterns across the signal. For instance, you have a voice signal and you have a convolutional layer. Each convolution traverses the voice to find meaningful patterns by employing a cost function.

**Conv2D** is used for images. This use case is very popular. The convolution method used for this layer is so called *convolution over volume*. This means you have a two-dimensional image which contains multiple channels, RGB as an example. In this case, each convolutional filter should be a three-dimensional filter to be convolved, cross-correlated actually, with the image to find appropriate patterns across the image.

**Conv3D** is usually used for videos where you have a frame for each time span. These layers usually have more parameters to be learnt than the previous layers. The reason we call them *$3D$* is that other than images for each frame, there is another axis called *time* containing discrete values, and each of them corresponds to a particular frame.


===========================================================================

## dimensionality of the input space

https://datascience.stackexchange.com/a/51609/101199

The only difference is the dimensionality of the input space. The input for a convolutional layer has the following shape:

input_shape = (batch_size,input_dims,channels)

* Input shape for **conv1D**: (batch_size,W,channels)

  Example: 1 second stereo voice signal sampled at 44100 Hz, shape: (batch_size,44100,2)

* Input shape for **conv2D**: (batch_size,(H,W),channels)

  Example: 32x32 RGB image, shape: (batch_size,32,32,3)

* Input shape for **conv3D**: (batch_size,(H,w,D),channels)

  Example (more tricky): 1 second video of 32x32 RGB images at 24 fps, shape: (batch_size,32,32,3,24)

**What a channel is?**

The key thing is to think about what the channel means for our input data. The convolutional layer apply different filters for each channel, thus, the weights of the conv layer have the following shape:

    (kernel_size,num_channels,num_filter_per_channels)

Example:

Convolutional layer with 12 filters and square kernel matrix of size of 3. This layer will apply 12 different filters for each channel. In the examples given previously:

* 1 second stereo voice signal sampled at 44100 Hz, kernel_size = 3

  12 x 2 = 24 one-dimensional filters, 12 filter for each channel

         Weigths shape: (3, 2, 12)

* 32x32 RGB image, kernel_size = (3,3)

  12 x 3 = **36 two-dimensional filters, 12 filter for each channel**

        Weights shape: (3, 3, 3, 12)


* 1 second video of 32x32 RGB images at 24 fps, kernel_size = (3,3,3)

  24 x 12 = 288 three-dimensional filters, 12 filter for each channel

        Weights shape: (3, 3, 3, 24, 12)


===========================================================================

Why do we use 2D convolution for RGB images which are in fact 3D data cubes?

https://www.quora.com/Why-do-we-use-2D-convolution-for-RGB-images-which-are-in-fact-3D-data-cubes-Shouldnt-we-use-3D-convolution-for-RGB-and-4D-for-video


A 2-d convolution ‘convolves’ along two spatial dimensions. It has a really small kernel, essentially a window of pixel values, that slides along those two dimensions. The rgb channel is not handled as a small window of depth, but rather, is obtained from beginning to end, first channel to last. That is, even a convolution with a small spatial window of 1x1, which takes a single pixel spatially in the width/height dimensions, would still take all 3 RGB channels.

This becomes easier to visualize when you realize that further down in the architectural pipeline there will be volumes of small images (64x64) that contain say 512 or more channels of information. While the 2d convolutions are applied in the small window on the 64x64 surface, they take the entirety of the channels into consideration, all 512 of them.

A 3d convolution could be used instead of the 2d convolution, but that would only change the fact that instead of all channels being used during each convolutional computation step, only a small local window of channels would be used.

===========================================================================

## Why 3-D in Medial Image Segmentation

https://arxiv.org/pdf/2108.08467.pdf -

Though 2D CNN can learn the spatial relationship between the pixels in a 2D plane, it cannot
learn the inter-slice relationship between the frames. In medical images such as X-ray or Fundus images,
information is restricted into a 2D plane, and hence 2D convolutions can effectively detect relevant features.
However, in 3D volumetric data such as MRI, CT, or USG, the region of interest may spread across multiple
frames, and hence the inter-slice information became significant. In such cases, individual slice analysis with
2D convolutions cannot retrieve all useful information and may affect the segmentation performance. On
the other side, 3D CNN uses convolution kernels in three directions, and hence the inter-slice information
can be learned to provide better segmentation results.