# Brain MRI Segmentation

All images are provided in .tif format with 3 channels per image.
For 101 cases, 3 sequences are available, i.e. pre-contrast, FLAIR, post-contrast (in this order of channels).
For 9 cases, post-contrast sequence is missing and for 6 cases, pre-contrast sequence is missing.
Missing sequences are replaced with FLAIR sequence to make all images 3-channel.
Masks are binary, 1-channel images.
They segment FLAIR abnormality present in the FLAIR sequence (available for all cases).

#### The dataset is organized into 110 folders named after case ID that contains information about source institution. So Folders correspond to patients. Each folder consists of around 20 tif images and masks.

#### Images in folders correspond to slices.
Each folder contains MR images with the following naming convention:

TCGA_<institution-code>_<patient-id>_<slice-number>.tif

Corresponding masks have a _mask suffix.

===========================================================================

* Some images mask is about black screen with no tumor delineation for ex: TCGA_CS_4941_19960909_1_mask.tif - Because, not all slices contain tumor. Most of them don't.

Basically it means if the _mask.tif image corresponding to a only .tif image is empty / blank then this particular slice of image does not have a Tumor.

The below NB has a Code to get this information from _mask.tif image and create a 1/0 diagnosis column for Tumor Detection.

https://www.kaggle.com/code/bonhart/brain-mri-data-visualization-unet-fpn/notebook

```py
# Adding A/B column for diagnosis
def positive_negative_diagnosis(mask_path):
    value = np.max(cv2.imread(mask_path))
    if value > 0 : return 1
    else: return 0

df["diagnosis"] = df["mask_path"].apply(lambda m: positive_negative_diagnosis(m))

```

So `cv2.imread(mask_path)` will return a 3-D Tensor representing the image and then np.max() will get the max value of that Tensor.

0 represents black and 255 represents white.

* The images are a fusion of three MRI sequences.

===========================================================================

### [Kaggle Dataset](https://www.kaggle.com/code/monkira/brain-mri-segmentation-using-unet-keras/notebook)

Folder names contains "TCGA_CS_4941" , " TCGA_DU_5871" , "TCGA_EZ_7264" , "TCGA_FG_8189" , "TCGA_HT_7473" .
What is the meaning of CS, DU, EZ, HT, FG?

These code the source institution that provided MRI data: The final group of 110 patients was from the following 5 institutions: Thomas Jefferson University (TCGA-CS, 16 patients), Henry Ford Hospital (TCGA-DU, 45 patients), UNC (TCGA-EZ, 1 patient), Case Western (TCGA-FG, 14 patients), Case Western – St. Joseph’s (TCGA-HT, 34 patients) from TCGA LGG collection.

===========================================================================

What do the 3 input channels correspond to?

The input channels correspond to pre-contrast, FLAIR, and post-contrast MRI sequences.



===========================================================================

