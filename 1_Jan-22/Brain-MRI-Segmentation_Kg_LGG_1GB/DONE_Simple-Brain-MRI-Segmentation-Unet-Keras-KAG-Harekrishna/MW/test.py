def unet(input_size=(256, 256, 3)):
    input = Input(input_size)

    conv1 = Conv2d(64, (3, 3), padding="same")(input)
    bn1 = Activation("relu")(conv1)
    conv1 = Conv2d(64, (3, 3), padding="same")(bn1)
    bn1 = BatchNormalization(axis=3)(conv1)
    bn1 = Activation("relu")(bn1)
    pool1 = MaxPooling2d(pool_size=(2, 2))(bn1)

    conv2 = Conv2d(128, (3, 3), padding="same")(pool1)
    bn2 = Activation("relu")(conv1)
    conv2 = Conv2d(128, (3, 3), padding="same")(bn1)
    bn2 = BatchNormalization(axis=3)(conv1)
    bn2 = Activation("relu")(bn1)
    pool2 = MaxPooling2d(pool_size=(2, 2))(bn1)

    conv3 = Conv2d(256, (3, 3), padding="same")(pool2)
    bn3 = Activation("relu")(conv1)
    conv3 = Conv2d(64, (3, 3), padding="same")(bn1)
    bn3 = BatchNormalization(axis=3)(conv1)
    bn3 = Activation("relu")(bn1)
    pool3 = MaxPooling2d(pool_size=(2, 2))(bn1)


def unet(train):
    train_generator