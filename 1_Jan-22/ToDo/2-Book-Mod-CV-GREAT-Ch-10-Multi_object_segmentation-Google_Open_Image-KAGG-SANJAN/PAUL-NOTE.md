Check the code here - 

/mnt/e0ccdbdb-22c3-4d9b-9413-fd976a2e99ae/M1/CV_Org/1_CV_SELECTED/1_REAL_CV_and_STARTER/Modern_Computer_Vision_with_Pytorch-2020-GREAT-SELECTED-WIP-Rs-3222/Modern-Computer-Vision-with-PyTorch-MY-Changes_Included/Chapter10/Multi_object_segmentation.ipynb

================================================


from torch_snippets import *

!wget -O train-annotations-object-segmentation.csv -q https://storage.googleapis.com/openimages/v5/train-annotations-object-segmentation.csv
!wget -O classes.csv -q https://raw.githubusercontent.com/openimages/dataset/master/dict.csv 


''' I have downloaded the above files manually in 

/mnt/e0ccdbdb-22c3-4d9b-9413-fd976a2e99ae/M1/CV_Org/Dataset/Google_Open_Image

'''
