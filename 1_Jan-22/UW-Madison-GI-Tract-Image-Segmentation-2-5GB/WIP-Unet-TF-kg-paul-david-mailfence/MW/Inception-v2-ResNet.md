https://towardsdatascience.com/a-simple-guide-to-the-versions-of-the-inception-network-7fc52b863202

Inception-ResNet v1 and v2
Inspired by the performance of the ResNet, a hybrid inception module was proposed. There are two sub-versions of Inception ResNet, namely v1 and v2. Before we checkout the salient features, let us look at the minor differences between these two sub-versions.

Inception-ResNet v1 has a computational cost that is similar to that of Inception v3.
Inception-ResNet v2 has a computational cost that is similar to that of Inception v4.
They have different stems, as illustrated in the Inception v4 section.
Both sub-versions have the same structure for the modules A, B, C and the reduction blocks. Only difference is the hyper-parameter settings. In this section, we’ll only focus on the structure. Refer to the paper for the exact hyper-parameter settings (The images are of Inception-Resnet v1).


## The Premise

Introduce residual connections that add the output of the convolution operation of the inception module, to the input.

## The Solution

For residual addition to work, the input and output after convolution must have the same dimensions. Hence, we use 1x1 convolutions after the original convolutions, to match the depth sizes (Depth is increased after convolution).