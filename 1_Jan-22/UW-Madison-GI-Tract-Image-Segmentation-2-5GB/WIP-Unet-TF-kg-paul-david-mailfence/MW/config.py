import os

BATCH_SIZE = 16
EPOCHS = 10
n_splits = 5
fold_selected = 2  # 1,...,5

TRAIN_ROOT_DIR = "../../input/uw-madison-gi-tract-image-segmentation/train"
TEST_ROOT_DIR = "../../input/uw-madison-gi-tract-image-segmentation/test"
