# TensorFlow - Protect the Great Barrier Reef - Competition Objective

 Competition Goal: accurately identify starfish (COTS - coral-eating Crown-Of-Thorns Starfish) in real-time by building an object detection model trained on underwater videos of coral reefs. This way we can help researchers & scientists to control COTS outbreaks, which are a threat to the Great Barrier Reef.

 A COTS outbreak can have devastating impacts to an entire coral reef, and depending on the event the ravenous starfish could wipe out nearly all living corals.

# Data Structuring and knowing the data

## [train.csv]

The `train.csv` dataset contains 5 columns that help identify the position within the video and sequence of the .jpg images within the `train_images` folder.

Additionaly, it has an `annotations` columns, which can be empty (`[]`) or could contain 1 or multiple coordinates for the location (or a bounding box) of the COTS.

![](https://i.imgur.com/xSuUaxf.png)

So the Video-ID can numbers between 0, 1 and 2, and the Training Images are also segregated accordingly, i.e. I have 3 Folders named video_0, video_1 and video_2


**video_id** - ID number of the video the image was part of. The video ids are not meaningfully ordered.

**video_frame** - The frame number of the image within the video. Expect to see occasional gaps in the frame number from when the diver surfaced.¶

**sequence** - ID of a gap-free subset of a given video. The sequence ids are not meaningfully ordered.

**sequence_frame** - The frame number within a given sequence.

**image_id** - ID code for the image, in the format '{video_id}-{video_frame}'

**annotations** - The bounding boxes of any starfish detections in a string format that can be evaluated directly with Python. Does not use the same format as the predictions you will submit. Not available in test.csv. A bounding box is described by the pixel coordinate (x_min, y_min) of its upper left corner within the image together with its width and height in pixels.

**example_sample_submission.csv** - A sample submission file in the correct format. The actual sample submission will be provided by the API; this is only provided to illustrate how to properly format predictions. The submission format is further described on the Evaluation page.

**example_test.npy** - Sample data that will be served by the example API.
