As the name indicates, LeNet5 has 5 layers with two convolutional and three fully connected layers.

Every convolutional layer includes three parts: convolution, pooling, and nonlinear activation functions

Let's start with the input. LeNet5 accepts as input a greyscale image of 32x32, indicating that the architecture is not suitable for RGB images (multiple channels). So the input image should contain just one channel. After this, we start with our convolutional layers.

The first convolutional layer has a filter size of  5x5 with 6 such filters. This will reduce the width and height of the image while increasing the depth (number of channels). The output would be 28x28x6.

After this, pooling is applied to decrease the feature map by half, i.e, 14x14x6.

For the next Layer, same filter size (5x5) with 16 filters is now applied to the output of previous layers > and then followed by a pooling layer. This reduces the output feature map to 5x5x16.


----------------------

## Script

Slide-1

The input for LeNet-5 is a 32×32 grayscale image which passes through the first convolutional layer with 6 feature maps or filters having size 5×5 and a stride of one. The image dimensions changes from 32x32x1 to 28x28x6.

And the 28*28 Output was arrived is 32 - 5 (Filter size) + 1 => 28

Trainable Parameters --

---------------------------

Slide 2

Then the LeNet-5 applies average pooling layer or sub-sampling layer with a filter size 2×2 and a stride of two. The resulting image dimensions will be reduced to 14x14x6.

Subsampling basically means Avg-Pooling or Max-Pooling. Here we are doing it for pool-size of 2. Means every 2*2 matrix of Pixel from the 28*28 becomes 1 pixel in the output.

And thats how 28*28 becomes 14*14


