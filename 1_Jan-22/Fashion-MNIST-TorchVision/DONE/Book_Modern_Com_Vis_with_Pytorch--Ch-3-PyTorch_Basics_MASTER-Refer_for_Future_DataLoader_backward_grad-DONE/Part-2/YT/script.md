### What is difference between nn.Module and nn.Sequential

[nn.Module](https://pytorch.org/docs/stable/generated/torch.nn.Module.html) is the base class for all neural network modules in PyTorch. As such [nn.Sequential](https://pytorch.org/docs/stable/generated/torch.nn.Sequential.html) is actually a direct subclass of nn.Module, you can look for yourself on this line.

When creating a new neural network, you would usually go about creating a new class and inheriting from nn.Module, and defining two methods: __init__ (the initializer, where you define your layers) and forward (the inference code of your module, where you use your layers). That's all you need, since PyTorch will handle backward pass with Autograd. Here is a example of a module:

```py

class NN(nn.Module):
    def __init__(self):
        super().__init__()

        self.fc1 = nn.Linear(10, 4)
        self.fc2 = nn.Linear(4, 2)

    def forward(self, x)
        x = F.ReLU(self.fc1(x))
        x = F.ReLU(self.fc2(x))
        return x

```

If the model you are defining is sequential, i.e. the layers are called iteratively on the input one by one. Then you can simply use a `nn.Sequential`. Because, `nn.Sequential` is a special kind of `nn.Module`.

So the equivalent code for the above `nn.Module` for `nn.Sequential` would be

```py
class NN(nn.Sequential):
    def __init__(self):
        super().__init__(
           nn.Linear(10, 4),
           nn.ReLU(),
           nn.Linear(4, 2),
           nn.ReLU())
```

Or even more simply

```py

NN = Sequential(
   nn.Linear(10, 4),
   nn.ReLU(),
   nn.Linear(4, 2),
   nn.Linear())

```

The objective of nn.Sequential is to quickly implement sequential modules such that you are not required to write the forward definition, it being implicitly known because the layers are sequentially called on the outputs.