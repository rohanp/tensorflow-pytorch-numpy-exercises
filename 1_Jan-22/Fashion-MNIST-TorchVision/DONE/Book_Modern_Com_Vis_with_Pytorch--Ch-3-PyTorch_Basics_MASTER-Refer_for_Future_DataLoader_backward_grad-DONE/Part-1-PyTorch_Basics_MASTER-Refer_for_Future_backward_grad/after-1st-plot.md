
#### In the above output plot of image -- each row represents a sample of 10 different images all belonging to the same class.

#### np.where => It outputs :

A. [ndarray or tuple of ndarrays] If both x and y are specified, the output array contains elements of x where condition is True, and elements from y elsewhere.

B. If only condition is given, which is the case here in the line `np.where(train_img_targets == label_class)`
which will return me the tuple( condition.nonzero(), the indices where condition is True ).


```py
a = np.array([[1, 2, 3], [4, 5, 6]])
b = np.where(a<4)
# => (array([0, 0, 0], dtype=int64), array([0, 1, 2], dtype=int64))

```

Note the above output is a Tuple ( condition.nonzero(), the indices where condition is True )


#### enumerate()

When you use enumerate(), the function gives you back two loop variables:

- The count of the current iteration
- The value of the item at the current iteration

---

**torch** is the main module that holds all the things you need for Tensor computation.

**`torch.nn and torch.nn.functional`** - The torch.nnmodule provides many classes and functions to build neural networks. You can think of it as the fundamental building blocks of neural networks: models, all kinds of layers, activation functions, parameter classes, etc. It allows us to build the model like putting some LEGO set together.
torch.optim

`torch.optim` offers all the optimizers like SGD, ADAM, etc., so you don’t have to write it from scratch.
torchvision

`torchvision` contains a lot of popular datasets, model architectures, and common image transformations for computer vision. We get our Fashion MNIST dataset from it and also use its transforms.

### Some basic functions in Pytorch

- `transforms.Compose` creates a series of transformation to prepare the dataset.
transforms.ToTenser convert PIL image(L, LA, P, I, F, RGB, YCbCr, RGBA, CMYK, 1) or numpy.ndarray (H x W x C) in the range [0, 255] to a torch.FloatTensor of shape (C x H x W) in the range [0.0, 1.0].

- `transform.Normalize` Normalize a tensor image with mean and standard deviation. Tensor image size should be (C x H x W) to be normalized which we already did usetransforms.ToTenser.

- `datasets.FashionMNIST` to download the Fashion MNIST datasets and transform the data. train=True if we want to get trained dataset otherwise set the parameter False for the test dataset.

- `torch.utils.data.Dataloader` takes our data train or test data with parameter batch_size and shuffle. batch_size define the how many samples per batch to load, and shuffle parameter set the True to have the data reshuffled at every epoch.

---

### Start of building the Neural Network

First building a class that fetches the dataset.

This class is derived from a Dataset class and needs three magic functions—

`__init__, __getitem__, and __len__` to always be defined:
