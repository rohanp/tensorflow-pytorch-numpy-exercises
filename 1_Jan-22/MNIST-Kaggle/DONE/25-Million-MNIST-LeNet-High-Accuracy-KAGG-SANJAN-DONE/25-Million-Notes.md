
Is it A convolution with stride 2 better than max pooling

People debate which is better. Using a convolution with stride 1 followed by max pooling produces nearly the same output as a convolution with stride 2. However there is a big difference in compute time. The former does twice as many computations and thus the later is twice as fast.

===========================================================================

## How do you know your data argument count is 25 million ?

https://www.kaggle.com/code/cdeotte/25-million-images-0-99757-mnist/comments


25,515,000 images = 42,000 images * 90% Train Split Dataset * 45 epochs * 15 CNN

- Kaggle's train.csv contains 42,000 images.
- Each epoch, 90% of them are transformed into new images by data augmentation. (10% are withheld for validation).
- Over 45 epochs, each CNN uses 42,000 * 0.9 * 45 = 1.7 million images!
- The ensemble consists of 15 CNNs, so the ensemble uses 1.7 * 15 = 25 million images!
