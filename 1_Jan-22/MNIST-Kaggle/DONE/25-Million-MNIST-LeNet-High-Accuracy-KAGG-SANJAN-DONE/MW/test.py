# DECREASE LEARNING RATE EACH EPOCH
lr_scheduler = LearningRateScheduler(lambda x: 1e-3 * 0.95 ** x)
# TRAIN NETWORKS
history = [0] * nets
epochs = 45
for cnn in range(nets):
    X_train_split, X_val_split, Y_train_split, Y_val_split = train_test_split(
        X_train, Y_train, test_size=0.1
    )
    history[cnn] = model[cnn].fit_generator(
        datagen.flow(X_train_split, Y_train_split, batch_size=64),
        epochs=epochs,
        steps_per_epoch=X_train_split.shape[0] // 64,
        validation_data=(X_val_split, Y_val_split),
        callbacks=[lr_scheduler],
        verbose=0,
    )
    print(
        "CNN {0:d}: Epochs={1:d}, Train accuracy={2:.5f}, Validation accuracy={3:.5f}".format(
            cnn + 1,
            epochs,
            max(history[cnn].history["acc"]),
            max(history[cnn].history["val_acc"]),
        )
    )
